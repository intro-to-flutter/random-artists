class Artist{
  String name;
  String imagePath;

  Artist( String name, String imagePath){
    this.name = name;
    this.imagePath = imagePath;
  }
}

Artist poet_1 = new Artist("Nazım Hikmet Ran", "images/poet_1.jpg");
Artist poet_2 = new Artist("Attila İlhan", "images/poet_2.jpg");
Artist poet_3 = new Artist("Turgut Uyar", "images/poet_3.jpg");
Artist poet_4 = new Artist("Necip Fazıl Kısakürek", "images/poet_4.jpg");
Artist poet_5 = new Artist("Can Yücel", "images/poet_5.jpg");

Map<String, Artist> poetMap = {
  "1": poet_1,
  "2": poet_2,
  "3": poet_3,
  "4": poet_4,
  "5": poet_5
};



var writer_1 = new Artist("Sabahattin Ali","images/writer_1.jpg");
var writer_2 = new Artist("Yasar Kemal","images/writer_2.jpg");
var writer_3 = new Artist("Zülfü Livaneli","images/writer_3.jpg");
var writer_4 = new Artist("Sait Faik Abasiyanik","images/writer_4.jpg");
var writer_5 = new Artist("Oguz Atay", "images/writer_5.jpg");

Map<String, Artist> writerMap = {
  "1": writer_1,
  "2": writer_2,
  "3": writer_3,
  "4": writer_4,
  "5": writer_5
};

List<Artist> writerList = [
  writer_1,
  writer_2,
  writer_3,
  writer_4,
  writer_5,
];


var actor_1 = new Artist("Kemal Sunal", "images/actor_1.jpg");
var actor_2 = new Artist("Tarık Akan", "images/actor_2.jpg");
var actor_3 = new Artist("Sener Sen", "images/actor_3.jpg");
var actor_4 = new Artist("Sadri Alisik", "images/actor_4.jpg");
var actor_5 = new Artist("Halit Akcatepe", "images/actor_5.jpg");

Map<String, Artist> actorMap = {
  "1": actor_1,
  "2": actor_2,
  "3": actor_3,
  "4": actor_4,
  "5": actor_5
};

List<Artist> actorList = [
  actor_1,
  actor_2,
  actor_3,
  actor_4,
  actor_5,
];