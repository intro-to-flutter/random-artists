import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:random_artists/data.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Center(
                child: Text(
              "Random Artists",
              style: GoogleFonts.calligraffitti(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 30),
            )),
            backgroundColor: Colors.green[300],
          ),
          body: Artists()),
    );
  }
}

class Artists extends StatefulWidget {
  @override
  _ArtistsState createState() => _ArtistsState();
}

class _ArtistsState extends State<Artists> {
  final random = new Random();
  int i = 1;

  void getRandomId() {
    setState(() {
      i = random.nextInt(5) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.green[100],
        child: Center(
          child: Column(
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: FlatButton(
                  highlightColor: Colors.green[100],
                  splashColor: Colors.green[100],
                  onPressed: getRandomId,
                  child: CircleAvatar(
                      radius: 90,
                      backgroundImage:
                          AssetImage(poetMap[i.toString()].imagePath)),
                ),
              )),
              Text(
                poetMap[i.toString()].name,
                style: GoogleFonts.calligraffitti(fontWeight: FontWeight.bold),
              ),
              Container(
                  width: 200,
                  child: Divider(
                    thickness: 1,
                  )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: FlatButton(
                  highlightColor: Colors.green[100],
                  splashColor: Colors.green[100],
                  onPressed: getRandomId,
                  child: CircleAvatar(
                      radius: 90,
                      backgroundImage: AssetImage(
                          writerMap[i.toString()].imagePath)),
                ),
              )),
              Text(
                writerMap[i.toString()].name,
                style: GoogleFonts.calligraffitti(fontWeight: FontWeight.bold),
              ),
              Container(
                  width: 200,
                  child: Divider(
                    thickness: 1,
                  )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: FlatButton(
                  highlightColor: Colors.green[100],
                  splashColor: Colors.green[100],
                  onPressed: getRandomId,
                  child: CircleAvatar(
                      radius: 90,
                      backgroundImage: AssetImage(
                          actorMap[i.toString()].imagePath)),
                ),
              )),
              Text(
                actorMap[i.toString()].name,
                style: GoogleFonts.calligraffitti(fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
